# Projet LabCRUDCICD

Ce projet contient une application Java pour faire le crud avec springboot et Java 17.
student crud

## GitLab CI/CD

Ce projet utilise GitLab CI/CD pour automatiser les tâches de build et de déploiement.

### Étapes du Pipeline

Le pipeline CI/CD est configuré avec les étapes suivantes :

1. **Packaging**: Cette étape compile le code source, exécute les tests unitaires et package l'application en un fichier JAR.
2. **Build Docker Image**: Cette étape construit une image Docker contenant l'application à partir d'un fichier Dockerfile.

### Status du Pipeline

Le statut du pipeline est affiché sur la page principale de ce projet dans GitLab. Vous pouvez suivre le progrès du pipeline et vérifier si toutes les étapes ont réussi.

## Exécution du Pipeline

Le pipeline CI/CD est déclenché automatiquement à chaque push sur la branche principale (main). Vous pouvez également déclencher manuellement le pipeline en appuyant sur le bouton "Run Pipeline" dans l'interface GitLab.

## Artifacts

À la fin de chaque exécution du pipeline, des artifacts sont générés et disponibles pour téléchargement. Ces artifacts comprennent le fichier JAR de l'application et une archive TAR contenant l'image Docker.

## Déploiement de l'Image Docker

L'image Docker construite est automatiquement poussée vers le Docker Hub à la fin du pipeline CI/CD. Vous pouvez ensuite déployer cette image Docker sur n'importe quel environnement compatible Docker en utilisant la commande `docker run`.

Pour plus d'informations sur la configuration du pipeline CI/CD, veuillez consulter le fichier `.gitlab-ci.yml` situé à la racine de ce projet.


## Image des configuration

