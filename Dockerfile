FROM openjdk:17-jdk-slim

LABEL maintainer="Youdev youssoudiarrandiaye52@gmail.com"

EXPOSE 8083

ADD target/lab-crud.jar lab-crud.jar

ENTRYPOINT ["java", "-jar", "lab-crud.jar"]